
Introduction
============

This is a Qt application and it uses qmake as part of the build process.
qmake is a makefile generator and is a part of your Qt installation.


Requirements
============

Qt development libraries Qt4 (minimum version 4.7.1) or Qt5 (minimum version 5.5.1) <https://www.qt.io/>

The GCC  C/C++ compiler.

Compiling and installing on Ubuntu Linux and derivatives:

sudo apt-get update

sudo apt-get install g++ make git qtbase5-dev-tools qtbase5-dev qt5-default


Build and run without "installing"
==================================

git clone https://gitlab.com/Teuniz/serial-com-tester.git

cd serial-com-tester

qmake

make -j8

Now you can run the program by typing ./sct


Installing
==========

Build the program like described before.
After compilation has successfully finished, enter the following command:

sudo make install

Now you can run the program by typing sct



