/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2007 - 2022 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/



#include "mainwindow.h"


#define SERIALPORTTESTER_TIMERVAL            (50)
#define SERIALPORTTESTER_S_BYTES_PER_ROW      (8)
#define INPMODE_ASCII                         (0)
#define INPMODE_HEX                           (1)
#define INPMODE_BIN                           (2)
#define SERIALPORTTESTER_MAXLISTROWS        (999)


// #define SERIALPORTTESTER_RX_DEBUG





UI_Mainwindow::UI_Mainwindow()
{
  int i;

  for(i=0; i<SERIALPORTTESTER_INPUT_HISTORY; i++)
  {
    input_history[i][0] = 0;
  }

  comport_nr = 0;

  port_open = 0;

  baudrate = 9600;

  newline = 0;

  history_idx = 0;

  prevline_idx = 0;

  inputmode = INPMODE_ASCII;

  strcpy(mode_str, "8N1");

  statDTR = 0;
  statRTS = 0;
  statDSR = 0;
  statCTS = 0;
  statDCD = 0;
  statRNG = 0;
  statBREAK = 0;

  appfont.setFamily("Noto Sans");
  appfont.setPixelSize(12);

  monofont.setFamily("mono");
  monofont.setPixelSize(12);

  dialog = new QDialog;

  dialog->setMinimumSize(660, 550);
  dialog->setMaximumSize(660, 550);
  dialog->setWindowTitle(PROGRAM_NAME " " PROGRAM_VERSION);
  dialog->setFont(appfont);

  setlocale(LC_NUMERIC, "C");

  DTRButton = new QPushButton(dialog);
  DTRButton->setText("DTR");
  DTRButton->setAutoDefault(false);
  DTRButton->setGeometry(10, 110, 60, 25);
  DTRButton->setEnabled(false);
  DTRButton->setToolTip("DTR (pin 4): This is an output.\n"
                        "Disabled: -3 to -15 Volt\n"
                        "Enabled: +3 to +15 Volt");

  RTSButton = new QPushButton(dialog);
  RTSButton->setText("RTS");
  RTSButton->setAutoDefault(false);
  RTSButton->setGeometry(10, 140, 60, 25);
  RTSButton->setEnabled(false);
  RTSButton->setToolTip("RTS (pin 7): This is an output.\n"
                        "Disabled: -3 to -15 Volt\n"
                        "Enabled: +3 to +15 Volt");

  BREAKButton = new QPushButton(dialog);
  BREAKButton->setText("BREAK");
  BREAKButton->setAutoDefault(false);
  BREAKButton->setGeometry(10, 170, 60, 25);
  BREAKButton->setEnabled(false);
  BREAKButton->setToolTip("TXD (pin 3): This is an output.\n"
                        "Disabled: -3 to -15 Volt\n"
                        "Enabled: +3 to +15 Volt");

  openButton = new QPushButton(dialog);
  openButton->setText("Open Port");
  openButton->setAutoDefault(false);
  openButton->setGeometry(10, 510, 100, 25);

  aboutButton = new QPushButton(dialog);
  aboutButton->setText("About");
  aboutButton->setAutoDefault(false);
  aboutButton->setGeometry(585, 510, 60, 25);

  portLabel = new QLabel(dialog);
  portLabel->setText("Port");
  portLabel->setGeometry(10, 20, 60, 25);
  portLabel->setAlignment(Qt::AlignHCenter);
  portLabel->setToolTip("In order to have access to the serial port, you need to be a member of the group \"dialout\".");

  baudrateLabel = new QLabel(dialog);
  baudrateLabel->setText("Baudrate");
  baudrateLabel->setGeometry(10, 50, 60, 25);
  baudrateLabel->setAlignment(Qt::AlignHCenter);
  baudrateLabel->setToolTip("Maximum baudrate with traditional Comports is 115200 baud.\n"
                            "Special cards or USB to Serial converters can usually be set to higher baudrates.");

  modeLabel = new QLabel(dialog);
  modeLabel->setText("Mode");
  modeLabel->setGeometry(10, 80, 60, 25);
  modeLabel->setAlignment(Qt::AlignHCenter);
  modeLabel->setToolTip("Databits (5-8), parity (No/Od/Even), stopbits (1-2)");

  DSRLabel = new QLabel(dialog);
  DSRLabel->setText("DSR");
  DSRLabel->setGeometry(10, 205, 60, 25);
  DSRLabel->setAlignment(Qt::AlignHCenter);
  DSRLabel->setToolTip("DSR (pin 6): This is an input.\n"
                       "Disabled: -3 to -15 Volt\n"
                       "Enabled: +3 to +15 Volt");

  CTSLabel = new QLabel(dialog);
  CTSLabel->setText("CTS");
  CTSLabel->setGeometry(10, 235, 60, 25);
  CTSLabel->setAlignment(Qt::AlignHCenter);
  CTSLabel->setToolTip("CTS (pin 8): This is an input.\n"
                       "Disabled: -3 to -15 Volt\n"
                       "Enabled: +3 to +15 Volt");

  DCDLabel = new QLabel(dialog);
  DCDLabel->setText("DCD");
  DCDLabel->setGeometry(10, 265, 60, 25);
  DCDLabel->setAlignment(Qt::AlignHCenter);
  DCDLabel->setToolTip("DCD (pin 1): This is an input.\n"
                       "Disabled: -3 to -15 Volt\n"
                       "Enabled: +3 to +15 Volt");

  RNGLabel = new QLabel(dialog);
  RNGLabel->setText("RING");
  RNGLabel->setGeometry(10, 295, 60, 25);
  RNGLabel->setAlignment(Qt::AlignHCenter);
  RNGLabel->setToolTip("RING (pin 9): This is an input.\n"
                       "Disabled: -3 to -15 Volt\n"
                       "Enabled: +3 to +15 Volt");

  newlineLabel = new QLabel(dialog);
  newlineLabel->setText("Append");
  newlineLabel->setGeometry(10, 335, 60, 25);
  newlineLabel->setAlignment(Qt::AlignHCenter);
  newlineLabel->setToolTip("Append a New-Line or Carriage-Return character when pressing Enter");

  comportComboBox = new QComboBox(dialog);
  comportComboBox->addItem("ttyS0");
  comportComboBox->addItem("ttyS1");
  comportComboBox->addItem("ttyS2");
  comportComboBox->addItem("ttyS3");
  comportComboBox->addItem("ttyS4");
  comportComboBox->addItem("ttyS5");
  comportComboBox->addItem("ttyS6");
  comportComboBox->addItem("ttyS7");
  comportComboBox->addItem("ttyS8");
  comportComboBox->addItem("ttyS9");
  comportComboBox->addItem("ttyS10");
  comportComboBox->addItem("ttyS11");
  comportComboBox->addItem("ttyS12");
  comportComboBox->addItem("ttyS13");
  comportComboBox->addItem("ttyS14");
  comportComboBox->addItem("ttyS15");
  comportComboBox->addItem("ttyUSB0");
  comportComboBox->addItem("ttyUSB1");
  comportComboBox->addItem("ttyUSB2");
  comportComboBox->addItem("ttyUSB3");
  comportComboBox->addItem("ttyUSB4");
  comportComboBox->addItem("ttyUSB5");
  comportComboBox->addItem("ttyAMA0");
  comportComboBox->addItem("ttyAMA1");
  comportComboBox->addItem("ttyACM0");
  comportComboBox->addItem("ttyACM1");
  comportComboBox->addItem("rfcomm0");
  comportComboBox->addItem("rfcomm1");
  comportComboBox->addItem("ircomm0");
  comportComboBox->addItem("ircomm1");
  comportComboBox->setToolTip("In order to have access to the serial port, you need to be a member of the group \"dialout\".");
  comportComboBox->setGeometry(100, 20, 80, 25);
  comportComboBox->setCurrentIndex(16);

  baudrateComboBox = new QComboBox(dialog);
  baudrateComboBox->addItem("50");
  baudrateComboBox->addItem("75");
  baudrateComboBox->addItem("110");
  baudrateComboBox->addItem("134");
  baudrateComboBox->addItem("150");
  baudrateComboBox->addItem("200");
  baudrateComboBox->addItem("300");
  baudrateComboBox->addItem("600");
  baudrateComboBox->addItem("1200");
  baudrateComboBox->addItem("1800");
  baudrateComboBox->addItem("2400");
  baudrateComboBox->addItem("4800");
  baudrateComboBox->addItem("9600");
  baudrateComboBox->addItem("19200");
  baudrateComboBox->addItem("38400");
  baudrateComboBox->addItem("57600");
  baudrateComboBox->addItem("115200");
  baudrateComboBox->addItem("230400");
  baudrateComboBox->addItem("460800");
  baudrateComboBox->addItem("500000");
  baudrateComboBox->addItem("576000");
  baudrateComboBox->addItem("921600");
  baudrateComboBox->addItem("1000000");
  baudrateComboBox->addItem("1152000");
  baudrateComboBox->addItem("1500000");
  baudrateComboBox->addItem("2000000");
  baudrateComboBox->addItem("2500000");
  baudrateComboBox->addItem("3000000");
  baudrateComboBox->addItem("3500000");
  baudrateComboBox->addItem("4000000");
  baudrateComboBox->setCurrentIndex(12);
  baudrateComboBox->setGeometry(100, 50, 80, 25);
  baudrateComboBox->setToolTip("Maximum baudrate with traditional UART's is 115200 baud.\n"
                               "Special cards or USB to Serial converters can usually be set to higher baudrates.");

  modeComboBox = new QComboBox(dialog);
  modeComboBox->addItem("5N1");
  modeComboBox->addItem("6N1");
  modeComboBox->addItem("7N1");
  modeComboBox->addItem("8N1");
  modeComboBox->addItem("5E1");
  modeComboBox->addItem("6E1");
  modeComboBox->addItem("7E1");
  modeComboBox->addItem("8E1");
  modeComboBox->addItem("5O1");
  modeComboBox->addItem("6O1");
  modeComboBox->addItem("7O1");
  modeComboBox->addItem("8O1");
  modeComboBox->addItem("5N2");
  modeComboBox->addItem("6N2");
  modeComboBox->addItem("7N2");
  modeComboBox->addItem("8N2");
  modeComboBox->addItem("5E2");
  modeComboBox->addItem("6E2");
  modeComboBox->addItem("7E2");
  modeComboBox->addItem("8E2");
  modeComboBox->addItem("5O2");
  modeComboBox->addItem("6O2");
  modeComboBox->addItem("7O2");
  modeComboBox->addItem("8O2");
  modeComboBox->setCurrentIndex(3);
  modeComboBox->setGeometry(100, 80, 80, 25);
  modeComboBox->setToolTip("Databits (5-8), parity (No/Od/Even), stopbits (1-2)");

  newlineComboBox = new QComboBox(dialog);
  newlineComboBox->addItem("none");
  newlineComboBox->addItem("LF");
  newlineComboBox->addItem("CR");
  newlineComboBox->addItem("CR + LF");
  newlineComboBox->setCurrentIndex(0);
  newlineComboBox->setGeometry(100, 330, 80, 25);
  newlineComboBox->setToolTip("Append a New-Line or Carriage-Return character when pressing Enter");

  ledDTR = new TLed(dialog);
  ledDTR->setValue(false);
  ledDTR->setOnColor(Qt::red);
  ledDTR->setOffColor(Qt::darkRed);
  ledDTR->setGeometry(100, 115, 15, 15);
  ledDTR->setToolTip("DTR (pin 4): This is an output.\n"
                     "Disabled: -3 to -15 Volt\n"
                     "Enabled: +3 to +15 Volt");

  ledRTS = new TLed(dialog);
  ledRTS->setValue(false);
  ledRTS->setOnColor(Qt::red);
  ledRTS->setOffColor(Qt::darkRed);
  ledRTS->setGeometry(100, 145, 15, 15);
  ledRTS->setToolTip("RTS (pin 7): This is an output.\n"
                     "Disabled: -3 to -15 Volt\n"
                     "Enabled: +3 to +15 Volt");

  ledBREAK = new TLed(dialog);
  ledBREAK->setValue(false);
  ledBREAK->setOnColor(Qt::red);
  ledBREAK->setOffColor(Qt::darkRed);
  ledBREAK->setGeometry(100, 175, 15, 15);
  ledBREAK->setToolTip("TXD (pin 3): This is an output.\n"
                     "Disabled: -3 to -15 Volt\n"
                     "Enabled: +3 to +15 Volt");

  ledDSR = new TLed(dialog);
  ledDSR->setValue(false);
  ledDSR->setOnColor(Qt::yellow);
  ledDSR->setOffColor(Qt::darkYellow);
  ledDSR->setGeometry(100, 205, 15, 15);
  ledDSR->setToolTip("DSR (pin 6): This is an input.\n"
                     "Disabled: -3 to -15 Volt\n"
                     "Enabled: +3 to +15 Volt");

  ledCTS = new TLed(dialog);
  ledCTS->setValue(false);
  ledCTS->setOnColor(Qt::yellow);
  ledCTS->setOffColor(Qt::darkYellow);
  ledCTS->setGeometry(100, 235, 15, 15);
  ledCTS->setToolTip("CTS (pin 8): This is an input.\n"
                     "Disabled: -3 to -15 Volt\n"
                     "Enabled: +3 to +15 Volt");

  ledDCD = new TLed(dialog);
  ledDCD->setValue(false);
  ledDCD->setOnColor(Qt::yellow);
  ledDCD->setOffColor(Qt::darkYellow);
  ledDCD->setGeometry(100, 265, 15, 15);
  ledDCD->setToolTip("DCD (pin 1): This is an input.\n"
                     "Disabled: -3 to -15 Volt\n"
                     "Enabled: +3 to +15 Volt");

  ledRNG = new TLed(dialog);
  ledRNG->setValue(false);
  ledRNG->setOnColor(Qt::yellow);
  ledRNG->setOffColor(Qt::darkYellow);
  ledRNG->setGeometry(100, 295, 15, 15);
  ledRNG->setToolTip("RING (pin 9): This is an input.\n"
                     "Disabled: -3 to -15 Volt\n"
                     "Enabled: +3 to +15 Volt");

  rx_table = new QTableWidget(dialog);
  rx_table->setGeometry(200, 20, 445, 260);
  rx_table->setColumnCount(3);
  rx_table->setRowCount(0);
  rx_table->setSortingEnabled(false);
  rx_table->setSelectionBehavior(QAbstractItemView::SelectItems);
  rx_table->setSelectionMode(QAbstractItemView::SingleSelection);
  QStringList horizontallabels;
  horizontallabels += "Time";
  horizontallabels += "Received Data";
  horizontallabels += "ASCII";
  rx_table->setHorizontalHeaderLabels(horizontallabels);
  rx_table->setColumnWidth(0, 75);
  rx_table->setColumnWidth(1, 205);
  rx_table->setColumnWidth(2, 90);

  tx_table = new QTableWidget(dialog);
  tx_table->setGeometry(200, 300, 445, 150);
  tx_table->setColumnCount(3);
  tx_table->setRowCount(0);
  tx_table->setSortingEnabled(false);
  tx_table->setSelectionBehavior(QAbstractItemView::SelectItems);
  tx_table->setSelectionMode(QAbstractItemView::SingleSelection);
  QStringList horizontallabels2;
  horizontallabels2 += "Time";
  horizontallabels2 += "Transmitted Data";
  horizontallabels2 += "ASCII";
  tx_table->setHorizontalHeaderLabels(horizontallabels2);
  tx_table->setColumnWidth(0, 75);
  tx_table->setColumnWidth(1, 205);
  tx_table->setColumnWidth(2, 90);

  arrow_up_act = new QAction("^", this);
  arrow_up_act->setShortcut(QKeySequence::MoveToPreviousLine);

  arrow_down_act = new QAction("v", this);
  arrow_down_act->setShortcut(QKeySequence::MoveToNextLine);

  clear_tx_list_act = new QAction("Clear List", tx_table);
  tx_table->setContextMenuPolicy(Qt::ActionsContextMenu);
  tx_table->insertAction(NULL, clear_tx_list_act);

  clear_rx_list_act = new QAction("Clear List", rx_table);
  rx_table->setContextMenuPolicy(Qt::ActionsContextMenu);
  rx_table->insertAction(NULL, clear_rx_list_act);

  inp_lineedit = new QLineEdit(dialog);
  inp_lineedit->setGeometry(200, 470, 445, 25);
  inp_lineedit->setEnabled(false);
  inp_lineedit->setMaxLength(SERIALPORTTESTER_RCV_BUFSIZE / 4);
  inp_lineedit->insertAction(NULL, arrow_up_act);
  inp_lineedit->insertAction(NULL, arrow_down_act);
  inp_lineedit->setToolTip("Enter data here.\n"
                           "Use the up and down key's to scroll through previously typed data.");

  asciiRadioButton = new QRadioButton("ASCII", dialog);
  asciiRadioButton->setAutoExclusive(true);
  asciiRadioButton->setGeometry(200, 510, 80, 25);
  asciiRadioButton->setChecked(true);
  asciiRadioButton->setEnabled(false);
  asciiRadioButton->setToolTip("Enter data in plain text.");

  hexRadioButton = new QRadioButton("hex", dialog);
  hexRadioButton->setAutoExclusive(true);
  hexRadioButton->setGeometry(280, 510, 80, 25);
  hexRadioButton->setChecked(false);
  hexRadioButton->setEnabled(false);
  hexRadioButton->setToolTip("Enter data in hexadecimal form e.g.: a3ffe8");

  binRadioButton = new QRadioButton("bin", dialog);
  binRadioButton->setAutoExclusive(true);
  binRadioButton->setGeometry(360, 510, 80, 25);
  binRadioButton->setChecked(false);
  binRadioButton->setEnabled(false);
  binRadioButton->setToolTip("Enter data in binary form e.g.: 01001101");

  repeatCheckBox = new QCheckBox("Repeat last line every", dialog);
  repeatCheckBox->setGeometry(10, 390, 160, 25);
  repeatCheckBox->setTristate(false);
  repeatCheckBox->setCheckState(Qt::Unchecked);
  repeatCheckBox->setEnabled(false);

  repeatSpinBox = new QSpinBox(dialog);
  repeatSpinBox->setGeometry(10, 420, 120, 25);
  repeatSpinBox->setRange(50, 10000);
  repeatSpinBox->setSingleStep(10);
  repeatSpinBox->setSuffix(" mSec");
  repeatSpinBox->setValue(1000);

  t1 = new QTimer(dialog);

  repeatTimer = new QTimer(dialog);

  rx_buf = (char *)malloc(SERIALPORTTESTER_RCV_BUFSIZE + 8);
  im_buf = (char *)malloc(SERIALPORTTESTER_RCV_BUFSIZE + 8);
  tx_buf = (char *)malloc(SERIALPORTTESTER_RCV_BUFSIZE + 8);

  QObject::connect(t1,                SIGNAL(timeout()),                this, SLOT(timer1_handler()));
  QObject::connect(DTRButton,         SIGNAL(clicked()),                this, SLOT(DTRbutton_clicked()));
  QObject::connect(RTSButton,         SIGNAL(clicked()),                this, SLOT(RTSbutton_clicked()));
  QObject::connect(BREAKButton,       SIGNAL(clicked()),                this, SLOT(BREAKbutton_clicked()));
  QObject::connect(dialog,            SIGNAL(destroyed(QObject *)),     this, SLOT(dialog_destroyed(QObject *)));
  QObject::connect(inp_lineedit,      SIGNAL(returnPressed()),          this, SLOT(lineedit_return()));
  QObject::connect(newlineComboBox,   SIGNAL(currentIndexChanged(int)), this, SLOT(newline_changed(int)));
  QObject::connect(arrow_up_act,      SIGNAL(triggered()),              this, SLOT(getPreviousLine()));
  QObject::connect(arrow_down_act,    SIGNAL(triggered()),              this, SLOT(getNextLine()));
  QObject::connect(openButton,        SIGNAL(clicked()),                this, SLOT(openbutton_clicked()));
  QObject::connect(clear_tx_list_act, SIGNAL(triggered(bool)),          this, SLOT(clear_tx_list()));
  QObject::connect(clear_rx_list_act, SIGNAL(triggered(bool)),          this, SLOT(clear_rx_list()));
  QObject::connect(aboutButton,       SIGNAL(clicked()),                this, SLOT(show_about_dialog()));
  QObject::connect(asciiRadioButton,  SIGNAL(toggled(bool)),            this, SLOT(asciiRadioButtonToggled(bool)));
  QObject::connect(hexRadioButton,    SIGNAL(toggled(bool)),            this, SLOT(hexRadioButtonToggled(bool)));
  QObject::connect(binRadioButton,    SIGNAL(toggled(bool)),            this, SLOT(binRadioButtonToggled(bool)));
  QObject::connect(inp_lineedit,      SIGNAL(textEdited(QString)),      this, SLOT(txtEdited(QString)));
  QObject::connect(repeatTimer,       SIGNAL(timeout()),                this, SLOT(repeattimer_handler()));
  QObject::connect(repeatCheckBox,    SIGNAL(stateChanged(int)),        this, SLOT(repeatCheckBoxChanged(int)));
  QObject::connect(repeatSpinBox,     SIGNAL(valueChanged(int)),        this, SLOT(repeatSpinBoxChanged(int)));

  dialog->show();

  rx_table->setFocus();
}


void UI_Mainwindow::dialog_destroyed(QObject *)
{
  t1->stop();
  repeatTimer->stop();

  if(port_open)
  {
    RS232_disableDTR(comport_nr);
    RS232_disableDTR(comport_nr);
    RS232_CloseComport(comport_nr);
  }

  free(rx_buf);
  free(im_buf);
  free(tx_buf);
}


void UI_Mainwindow::repeatCheckBoxChanged(int stat)
{
  if(stat == Qt::Unchecked)
  {
    repeatTimer->stop();
  }
  else
  {
    repeatTimer->start(repeatSpinBox->value());
  }
}


void UI_Mainwindow::repeatSpinBoxChanged(int val)
{
  if(repeatCheckBox->isChecked() == true)
  {
    repeatTimer->start(val);
  }
}


void UI_Mainwindow::repeattimer_handler()
{
  int n, idx;

  if(!port_open)  return;

  if(statBREAK)  return;

  idx = history_idx - 1 + SERIALPORTTESTER_INPUT_HISTORY;

  idx %= SERIALPORTTESTER_INPUT_HISTORY;

  n = strlen(input_history[idx]);

  if(n > 0)
  {
    strcpy(im_buf, input_history[idx]);

    send_buf();
  }
}


void UI_Mainwindow::getPreviousLine()
{
  int idx;

  char tmp_str[SERIALPORTTESTER_RCV_BUFSIZE + 32];

  if(prevline_idx >= (SERIALPORTTESTER_INPUT_HISTORY - 1))  return;

  prevline_idx++;

  idx = history_idx - prevline_idx + SERIALPORTTESTER_INPUT_HISTORY;

  idx %= SERIALPORTTESTER_INPUT_HISTORY;

  if(inputmode == INPMODE_ASCII)
  {
    strcpy(tmp_str, input_history[idx]);

    hextoascii(tmp_str);

    inp_lineedit->setText(QString::fromLatin1(tmp_str));
  }
  else if(inputmode == INPMODE_BIN)
    {
      hextobin(tmp_str, input_history[idx]);

      inp_lineedit->setText(QString::fromLatin1(tmp_str));
    }
    else
    {
      inp_lineedit->setText(QString::fromLatin1(input_history[idx]));
    }
}


void UI_Mainwindow::getNextLine()
{
  int idx;

  char tmp_str[SERIALPORTTESTER_RCV_BUFSIZE + 32];

  if(prevline_idx < 1)  return;

  prevline_idx--;

  idx = history_idx - prevline_idx + SERIALPORTTESTER_INPUT_HISTORY;

  idx %= SERIALPORTTESTER_INPUT_HISTORY;

  if(inputmode == INPMODE_ASCII)
  {
    strcpy(tmp_str, input_history[idx]);

    hextoascii(tmp_str);

    inp_lineedit->setText(QString::fromLatin1(tmp_str));
  }
  else if(inputmode == INPMODE_BIN)
    {
      hextobin(tmp_str, input_history[idx]);

      inp_lineedit->setText(QString::fromLatin1(tmp_str));
    }
    else
    {
      inp_lineedit->setText(QString::fromLatin1(input_history[idx]));
    }
}


void UI_Mainwindow::timer1_handler()
{
  int status;

  if(!port_open)  return;

  status = RS232_IsDSREnabled(comport_nr);

  if(status != statDSR)
  {
    statDSR = status;

    if(statDSR)
    {
      ledDSR->setValue(true);
    }
    else
    {
      ledDSR->setValue(false);
    }
  }

  status = RS232_IsCTSEnabled(comport_nr);

  if(status != statCTS)
  {
    statCTS = status;

    if(statCTS)
    {
      ledCTS->setValue(true);
    }
    else
    {
      ledCTS->setValue(false);
    }
  }

  status = RS232_IsDCDEnabled(comport_nr);

  if(status != statDCD)
  {
    statDCD = status;

    if(statDCD)
    {
      ledDCD->setValue(true);
    }
    else
    {
      ledDCD->setValue(false);
    }
  }

  status = RS232_IsRINGEnabled(comport_nr);

  if(status != statRNG)
  {
    statRNG = status;

    if(statRNG)
    {
      ledRNG->setValue(true);
    }
    else
    {
      ledRNG->setValue(false);
    }
  }

  int n = RS232_PollComport(comport_nr, (unsigned char *)rx_buf, SERIALPORTTESTER_RCV_BUFSIZE);

#ifndef SERIALPORTTESTER_RX_DEBUG
  if(n == 0)  return;
#endif

  if(n < 0)
  {
    t1->stop();
    repeatTimer->stop();
    repeatCheckBox->setCheckState(Qt::Unchecked);

    QMessageBox msgBox;
    msgBox.setText("An error occurred while reading from serial port.");
    msgBox.exec();

    RS232_disableDTR(comport_nr);
    RS232_disableRTS(comport_nr);
    RS232_CloseComport(comport_nr);

    statDTR = 0;
    statRTS = 0;
    statDSR = 0;
    statCTS = 0;
    statDCD = 0;
    statRNG = 0;
    statBREAK = 0;

    ledDTR->setValue(false);
    ledRTS->setValue(false);
    ledBREAK->setValue(false);
    ledDSR->setValue(false);
    ledCTS->setValue(false);
    ledDCD->setValue(false);
    ledRNG->setValue(false);

    port_open = 0;

    baudrateComboBox->setEnabled(true);
    modeComboBox->setEnabled(true);
    comportComboBox->setEnabled(true);

    openButton->setText("Open Port");

    DTRButton->setEnabled(false);
    RTSButton->setEnabled(false);
    BREAKButton->setEnabled(false);
    inp_lineedit->setEnabled(false);
    asciiRadioButton->setEnabled(false);
    hexRadioButton->setEnabled(false);
    binRadioButton->setEnabled(false);
    repeatCheckBox->setEnabled(false);

    return;
  }

  int i, tmp, row;

  char hex_str[(SERIALPORTTESTER_S_BYTES_PER_ROW * 3) + 2],
       ascii_str[SERIALPORTTESTER_S_BYTES_PER_ROW + 2],
       time_str[16];

  QTableWidgetItem *item;

  struct tm *date_time;

  time_t elapsed_time;

  elapsed_time = time(NULL);
  date_time = localtime(&elapsed_time);

  sprintf(time_str, "%02i:%02i:%02i", date_time->tm_hour, date_time->tm_min, date_time->tm_sec % 60);

#ifdef SERIALPORTTESTER_RX_DEBUG
  n = rand();

  n %= 23;

  if(n < 1)  return;

  for(i=0; i<n; i++)
  {
    rx_buf[i] = rand();
  }
#endif

  row = rx_table->rowCount();

  while(row >= SERIALPORTTESTER_MAXLISTROWS)
  {
    rx_table->removeRow(0);

    row--;
  }

  for(i=0; i<n; i++)
  {
    tmp = ((unsigned char *)rx_buf)[i] / 16;
    if(tmp < 10)
    {
      tmp += 48;
    }
    else
    {
      tmp += 55;
    }

    hex_str[(i % SERIALPORTTESTER_S_BYTES_PER_ROW) * 3] = tmp;

    tmp = ((unsigned char *)rx_buf)[i] % 16;
    if(tmp < 10)
    {
      tmp += 48;
    }
    else
    {
      tmp += 55;
    }

    hex_str[((i % SERIALPORTTESTER_S_BYTES_PER_ROW) * 3) + 1] = tmp;

    hex_str[((i % SERIALPORTTESTER_S_BYTES_PER_ROW) * 3) + 2] = ' ';

    if((((unsigned char *)rx_buf)[i] < 32) || (((unsigned char *)rx_buf)[i] > 126))
    {
      tmp = '.';
    }
    else
    {
      tmp = rx_buf[i];
    }

    ascii_str[i % SERIALPORTTESTER_S_BYTES_PER_ROW] = tmp;

    if(!((i + 1) % SERIALPORTTESTER_S_BYTES_PER_ROW))
    {
      rx_table->setRowCount(row + 1);

      rx_table->setRowHeight(row, 18);

      item = new QTableWidgetItem(time_str);
      item->setFont(monofont);
      item->setFlags(Qt::ItemIsSelectable);
      rx_table->setItem(row, 0, item);

      hex_str[SERIALPORTTESTER_S_BYTES_PER_ROW * 3] = 0;

      item = new QTableWidgetItem;
      item->setFont(monofont);
      item->setFlags(Qt::ItemIsSelectable);
      item->setText(hex_str);
      rx_table->setItem(row, 1, item);

      ascii_str[SERIALPORTTESTER_S_BYTES_PER_ROW] = 0;

      item = new QTableWidgetItem;
      item->setFont(monofont);
      item->setFlags(Qt::ItemIsSelectable);
      item->setText(QString::fromLatin1(ascii_str));
      rx_table->setItem(row, 2, item);

      row++;
    }
  }

  if(i % SERIALPORTTESTER_S_BYTES_PER_ROW)
  {
    rx_table->setRowCount(row + 1);

    rx_table->setRowHeight(row, 18);

    item = new QTableWidgetItem(time_str);
    item->setFont(monofont);
    item->setFlags(Qt::ItemIsSelectable);
    rx_table->setItem(row, 0, item);

    hex_str[(i % SERIALPORTTESTER_S_BYTES_PER_ROW) * 3] = 0;

    item = new QTableWidgetItem;
    item->setFont(monofont);
    item->setFlags(Qt::ItemIsSelectable);
    item->setText(hex_str);
    rx_table->setItem(row, 1, item);

    ascii_str[i % SERIALPORTTESTER_S_BYTES_PER_ROW] = 0;

    item = new QTableWidgetItem;
    item->setFont(monofont);
    item->setFlags(Qt::ItemIsSelectable);
    item->setText(QString::fromLatin1(ascii_str));
    rx_table->setItem(row, 2, item);

    row++;
  }

  rx_table->scrollToBottom();
}


void UI_Mainwindow::lineedit_return()
{
  char buf[SERIALPORTTESTER_RCV_BUFSIZE];

  strcpy(buf, inp_lineedit->text().toLatin1().data());

  if(inputmode == INPMODE_ASCII)
  {
    asciitohex(im_buf, buf);

    send_buf();

    return;
  }
  else if(inputmode == INPMODE_BIN)
    {
      bintohex(buf);

      strcpy(im_buf, buf);

      send_buf();

      return;
    }

  strcpy(im_buf, buf);

  send_buf();
}


void UI_Mainwindow::send_buf(void)
{
  int i, j, k, n, tmp, tmp2, row;

  char hex_str[(SERIALPORTTESTER_S_BYTES_PER_ROW * 3) + 2],
       ascii_str[SERIALPORTTESTER_S_BYTES_PER_ROW + 2],
       time_str[16];

  QTableWidgetItem *item;


  prevline_idx = 0;

  if(newline == 1)
  {
    strcat(im_buf, "0a");
  }
  else if(newline == 2)
    {
      strcat(im_buf, "0d");
    }
    else if(newline == 3)
      {
        strcat(im_buf, "0d0a");
      }

  n = strlen(im_buf) / 2;

  if(n < 1)  return;

  if(newline == 0)
  {
    strcpy(input_history[history_idx++], im_buf);
  }
  else if(((newline == 1) || (newline == 2)) && (n > 1))
    {
      strcpy(input_history[history_idx], im_buf);

      input_history[history_idx++][(n*2)-2] = 0;
    }
    else if((newline == 3) && (n > 2))
      {
        strcpy(input_history[history_idx], im_buf);

        input_history[history_idx++][(n*2)-4] = 0;
      }

  history_idx %= SERIALPORTTESTER_INPUT_HISTORY;

  input_history[history_idx][0] = 0;

  for(i=1; i<SERIALPORTTESTER_INPUT_HISTORY; i++)
  {
    if(!(strcmp(input_history[(history_idx - 1 + SERIALPORTTESTER_INPUT_HISTORY) % SERIALPORTTESTER_INPUT_HISTORY],
          input_history[(history_idx - 1 - i + SERIALPORTTESTER_INPUT_HISTORY) % SERIALPORTTESTER_INPUT_HISTORY])))
    {
      for(j=i; j>0; j--)
      {
        strcpy(input_history[(history_idx - j - 1 + SERIALPORTTESTER_INPUT_HISTORY) % SERIALPORTTESTER_INPUT_HISTORY],
              input_history[(history_idx - j + SERIALPORTTESTER_INPUT_HISTORY) % SERIALPORTTESTER_INPUT_HISTORY]);
      }

      history_idx = history_idx - 1 + SERIALPORTTESTER_INPUT_HISTORY;

      history_idx %= SERIALPORTTESTER_INPUT_HISTORY;

      input_history[history_idx][0] = 0;

      break;
    }
  }

  struct tm *date_time;

  time_t elapsed_time;

  elapsed_time = time(NULL);
  date_time = localtime(&elapsed_time);

  sprintf(time_str, "%02i:%02i:%02i", date_time->tm_hour, date_time->tm_min, date_time->tm_sec % 60);

  row = tx_table->rowCount();

  while(row >= SERIALPORTTESTER_MAXLISTROWS)
  {
    tx_table->removeRow(0);

    row--;
  }

  for(i=0; i<n; i++)
  {
    hex_str[(i % SERIALPORTTESTER_S_BYTES_PER_ROW) * 3] = im_buf[i*2];

    hex_str[((i % SERIALPORTTESTER_S_BYTES_PER_ROW) * 3) + 1] = im_buf[(i*2)+1];

    hex_str[((i % SERIALPORTTESTER_S_BYTES_PER_ROW) * 3) + 2] = ' ';

    tmp = im_buf[i*2];
    if((tmp >= '0') && (tmp <= '9'))
    {
      tmp -= '0';
    }
    else if((tmp >= 'a') && (tmp <= 'f'))
      {
        tmp -= ('a' - 10);
      }
      else if((tmp >= 'A') && (tmp <= 'F'))
        {
          tmp -= ('A' - 10);
        }

    tmp *= 16;

    tmp2 = im_buf[(i*2)+1];
    if((tmp2 >= '0') && (tmp2 <= '9'))
    {
      tmp2 -= '0';
    }
    else if((tmp2 >= 'a') && (tmp2 <= 'f'))
      {
        tmp2 -= ('a' - 10);
      }
      else if((tmp2 >= 'A') && (tmp2 <= 'F'))
        {
          tmp2 -= ('A' - 10);
        }

    tmp += tmp2;

    tx_buf[i] = tmp;

    if((tmp < 32) || (tmp > 126))
    {
      ascii_str[i % SERIALPORTTESTER_S_BYTES_PER_ROW] = '.';
    }
    else
    {
      ascii_str[i % SERIALPORTTESTER_S_BYTES_PER_ROW] = tmp;
    }

    if(!((i + 1) % SERIALPORTTESTER_S_BYTES_PER_ROW))
    {
      tx_table->setRowCount(row + 1);

      tx_table->setRowHeight(row, 18);

      item = new QTableWidgetItem(time_str);
      item->setFont(monofont);
      item->setFlags(Qt::ItemIsSelectable);
      tx_table->setItem(row, 0, item);

      hex_str[SERIALPORTTESTER_S_BYTES_PER_ROW * 3] = 0;

      item = new QTableWidgetItem;
      item->setFont(monofont);
      item->setFlags(Qt::ItemIsSelectable);
      item->setText(hex_str);
      tx_table->setItem(row, 1, item);

      ascii_str[SERIALPORTTESTER_S_BYTES_PER_ROW] = 0;

      item = new QTableWidgetItem;
      item->setFont(monofont);
      item->setFlags(Qt::ItemIsSelectable);
      item->setText(QString::fromLatin1(ascii_str));
      tx_table->setItem(row, 2, item);

      row++;
    }
  }

  if(i % SERIALPORTTESTER_S_BYTES_PER_ROW)
  {
    tx_table->setRowCount(row + 1);

    tx_table->setRowHeight(row, 18);

    item = new QTableWidgetItem(time_str);
    item->setFont(monofont);
    item->setFlags(Qt::ItemIsSelectable);
    tx_table->setItem(row, 0, item);

    hex_str[(i % SERIALPORTTESTER_S_BYTES_PER_ROW) * 3] = 0;

    item = new QTableWidgetItem;
    item->setFont(monofont);
    item->setFlags(Qt::ItemIsSelectable);
    item->setText(hex_str);
    tx_table->setItem(row, 1, item);

    ascii_str[i % SERIALPORTTESTER_S_BYTES_PER_ROW] = 0;

    item = new QTableWidgetItem;
    item->setFont(monofont);
    item->setFlags(Qt::ItemIsSelectable);
    item->setText(QString::fromLatin1(ascii_str));
    tx_table->setItem(row, 2, item);

    row++;
  }

  tx_table->scrollToBottom();

  inp_lineedit->clear();

  k = RS232_SendBuf(comport_nr, (unsigned char *)tx_buf, n);

  if(k < 0)
  {
    t1->stop();
    repeatTimer->stop();
    repeatCheckBox->setCheckState(Qt::Unchecked);

    QMessageBox msgBox;
    msgBox.setText("An error occurred while writing to serial port.");
    msgBox.exec();

    RS232_disableDTR(comport_nr);
    RS232_disableRTS(comport_nr);
    RS232_CloseComport(comport_nr);

    statDTR = 0;
    statRTS = 0;
    statDSR = 0;
    statCTS = 0;
    statDCD = 0;
    statRNG = 0;

    ledDTR->setValue(false);
    ledRTS->setValue(false);
    ledDSR->setValue(false);
    ledCTS->setValue(false);
    ledDCD->setValue(false);
    ledRNG->setValue(false);

    port_open = 0;

    baudrateComboBox->setEnabled(true);
    modeComboBox->setEnabled(true);
    comportComboBox->setEnabled(true);

    openButton->setText("Open Port");

    DTRButton->setEnabled(false);
    RTSButton->setEnabled(false);
    inp_lineedit->setEnabled(false);
    asciiRadioButton->setEnabled(false);
    hexRadioButton->setEnabled(false);
    binRadioButton->setEnabled(false);
    repeatCheckBox->setEnabled(false);

    return;
  }
  else if(k == 0)
  {
    QMessageBox msgBox;
    msgBox.setText("Could no write to serial port.\n"
                   "Serial port reported: \"EAGAIN\"");
    msgBox.exec();
  }
}


void UI_Mainwindow::show_about_dialog()
{
  UI_Aboutwindow aboutwindow;
}


void UI_Mainwindow::openbutton_clicked()
{
  inp_lineedit->clear();

  if(port_open)
  {
    t1->stop();
    repeatTimer->stop();
    repeatCheckBox->setCheckState(Qt::Unchecked);

    RS232_disableDTR(comport_nr);
    RS232_disableRTS(comport_nr);
    RS232_CloseComport(comport_nr);

    statDTR = 0;
    statRTS = 0;
    statDSR = 0;
    statCTS = 0;
    statDCD = 0;
    statRNG = 0;
    statBREAK = 0;

    ledDTR->setValue(false);
    ledRTS->setValue(false);
    ledBREAK->setValue(false);
    ledDSR->setValue(false);
    ledCTS->setValue(false);
    ledDCD->setValue(false);
    ledRNG->setValue(false);

    port_open = 0;

    baudrateComboBox->setEnabled(true);
    modeComboBox->setEnabled(true);
    comportComboBox->setEnabled(true);

    openButton->setText("Open Port");

    DTRButton->setEnabled(false);
    RTSButton->setEnabled(false);
    inp_lineedit->setEnabled(false);
    asciiRadioButton->setEnabled(false);
    hexRadioButton->setEnabled(false);
    binRadioButton->setEnabled(false);
    repeatCheckBox->setEnabled(false);

    rx_table->setFocus();

    return;
  }

  statDTR = 0;
  statRTS = 0;
  statDSR = 0;
  statCTS = 0;
  statDCD = 0;
  statRNG = 0;
  statBREAK = 0;

  ledDTR->setValue(false);
  ledRTS->setValue(false);
  ledBREAK->setValue(false);
  ledDSR->setValue(false);
  ledCTS->setValue(false);
  ledDCD->setValue(false);
  ledRNG->setValue(false);

  comport_nr = comportComboBox->currentIndex();

  baudrateComboBox->setEnabled(false);
  modeComboBox->setEnabled(false);
  comportComboBox->setEnabled(false);

  clear_lists();

//   DTRButton->setEnabled(true);
//   RTSButton->setEnabled(true);
//   BREAKButton->setEnabled(true);
//   inp_lineedit->setEnabled(true);
//   asciiRadioButton->setEnabled(true);
//   hexRadioButton->setEnabled(true);
//   binRadioButton->setEnabled(true);
//   repeatCheckBox->setEnabled(true);

  baudrate = atoi(baudrateComboBox->currentText().toLatin1().data());

  strcpy(mode_str, modeComboBox->currentText().toLatin1().data());

  newline = newlineComboBox->currentIndex();

  if(RS232_OpenComport(comport_nr, baudrate, mode_str, 0))
  {
    QMessageBox msgBox;
    msgBox.setText("Can not open COM port.");
    msgBox.exec();

    baudrateComboBox->setEnabled(true);
    modeComboBox->setEnabled(true);
    comportComboBox->setEnabled(true);

//     DTRButton->setEnabled(false);
//     RTSButton->setEnabled(false);
//     BREAKButton->setEnabled(false);
//     asciiRadioButton->setEnabled(false);
//     hexRadioButton->setEnabled(false);
//     binRadioButton->setEnabled(false);
//     repeatCheckBox->setEnabled(false);
//     inp_lineedit->setEnabled(false);

    return;
  }

  DTRButton->setEnabled(true);
  RTSButton->setEnabled(true);
  BREAKButton->setEnabled(true);
  inp_lineedit->setEnabled(true);
  asciiRadioButton->setEnabled(true);
  hexRadioButton->setEnabled(true);
  binRadioButton->setEnabled(true);
  repeatCheckBox->setEnabled(true);

  port_open = 1;

  openButton->setText("Close Port");

  RS232_disableDTR(comport_nr);
  RS232_disableRTS(comport_nr);

  t1->start(SERIALPORTTESTER_TIMERVAL);

  inp_lineedit->setFocus();
}


void UI_Mainwindow::DTRbutton_clicked()
{
  if(!port_open)  return;

  if(statDTR == 0)
  {
    RS232_enableDTR(comport_nr);

    statDTR = 1;

    ledDTR->setValue(true);
  }
  else
  {
    RS232_disableDTR(comport_nr);

    statDTR = 0;

    ledDTR->setValue(false);
  }
}


void UI_Mainwindow::RTSbutton_clicked()
{
  if(!port_open)  return;

  if(statRTS == 0)
  {
    RS232_enableRTS(comport_nr);

    statRTS = 1;

    ledRTS->setValue(true);
  }
  else
  {
    RS232_disableRTS(comport_nr);

    statRTS = 0;

    ledRTS->setValue(false);
  }
}


void UI_Mainwindow::BREAKbutton_clicked()
{
  if(!port_open)  return;

  if(statBREAK == 0)
  {
    statBREAK = 1;

    RS232_flushTX(comport_nr);

    inp_lineedit->setEnabled(false);

    RS232_enableBREAK(comport_nr);

    ledBREAK->setValue(true);
  }
  else
  {
    RS232_disableBREAK(comport_nr);

    statBREAK = 0;

    ledBREAK->setValue(false);

    inp_lineedit->setEnabled(true);
  }
}


void UI_Mainwindow::clear_lists(void)
{
  clear_tx_list();
  clear_rx_list();
}


void UI_Mainwindow::clear_tx_list()
{
  tx_table->clear();

  QStringList horizontallabels;
  horizontallabels += "Time";
  horizontallabels += "Transmitted Data";
  horizontallabels += "ASCII";
  tx_table->setHorizontalHeaderLabels(horizontallabels);

  tx_table->setRowCount(0);
}


void UI_Mainwindow::clear_rx_list()
{
  rx_table->clear();

  QStringList horizontallabels;
  horizontallabels += "Time";
  horizontallabels += "Received Data";
  horizontallabels += "ASCII";
  rx_table->setHorizontalHeaderLabels(horizontallabels);

  rx_table->setRowCount(0);
}


void UI_Mainwindow::newline_changed(int idx)
{
  newline = idx;

  inp_lineedit->setFocus();
}


void UI_Mainwindow::asciiRadioButtonToggled(bool stat)
{
  if(stat == false)  return;

  inp_lineedit->clear();

  inputmode = INPMODE_ASCII;

  inp_lineedit->setFocus();
}


void UI_Mainwindow::hexRadioButtonToggled(bool stat)
{
  if(stat == false)  return;

  inp_lineedit->clear();

  inputmode = INPMODE_HEX;

  inp_lineedit->setFocus();
}


void UI_Mainwindow::binRadioButtonToggled(bool stat)
{
  if(stat == false)  return;

  inp_lineedit->clear();

  inputmode = INPMODE_BIN;

  inp_lineedit->setFocus();
}


void UI_Mainwindow::txtEdited(QString)  // input validator
{
  int i, j, len, csrpos, invalid=0;

  char str[SERIALPORTTESTER_RCV_BUFSIZE + 8];

  if(inputmode == INPMODE_ASCII)  return;

  strncpy(str, inp_lineedit->text().toLatin1().data(), SERIALPORTTESTER_RCV_BUFSIZE);

  str[SERIALPORTTESTER_RCV_BUFSIZE] = 0;

  len = strlen(str);

  if(len < 1)  return;

  csrpos = inp_lineedit->cursorPosition();

  if(inputmode == INPMODE_HEX)
  {
    for(i=0; i<len; i++)
    {
      if(((str[i] < 'a') || (str[i] > 'f')) && ((str[i] < 'A') || (str[i] > 'F')) && ((str[i] < '0') || (str[i] > '9')))
      {
        for(j=i; j<len; j++)
        {
          str[j] = str[j+1];
        }

        len--;

        invalid = 1;
      }
    }
  }

  if(inputmode == INPMODE_BIN)
  {
    for(i=0; i<len; i++)
    {
      if((str[i] != '0') && (str[i] != '1'))
      {
        for(j=i; j<len; j++)
        {
          str[j] = str[j+1];
        }

        len--;

        invalid = 1;
      }
    }
  }

  if(invalid)
  {
    inp_lineedit->setText(QString::fromLatin1(str));

    if(csrpos)  csrpos--;

    inp_lineedit->setCursorPosition(csrpos);
  }
}















