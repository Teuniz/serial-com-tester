
contains(QT_MAJOR_VERSION, 4) {

LIST = 0 1 2 3 4 5 6
for(a, LIST):contains(QT_MINOR_VERSION, $$a):error("This project needs Qt4 version >= 4.7.1 or Qt5 version >= 5.5.1")

contains(QT_MINOR_VERSION, 7) {
  LIST = 0
  for(a, LIST):contains(QT_PATCH_VERSION, $$a):error("This project needs Qt4 version >= 4.7.1 or Qt5 version >= 5.5.1")
}
}


contains(QT_MAJOR_VERSION, 5) {

LIST = 0 1 2 3 4
for(a, LIST):contains(QT_MINOR_VERSION, $$a):error("This project needs Qt4 version >= 4.7.1 or Qt5 version >= 5.5.1")

contains(QT_MINOR_VERSION, 5) {
  LIST = 0
  for(a, LIST):contains(QT_PATCH_VERSION, $$a):error("This project needs Qt4 version >= 4.7.1 or Qt5 version >= 5.5.1")
}
}


TEMPLATE = app
TARGET = sct
DEPENDPATH += .
INCLUDEPATH += .
CONFIG += qt
CONFIG += warn_on
CONFIG += release
CONFIG += static
CONFIG += largefile

contains(QT_MAJOR_VERSION, 5) {
QT += widgets
}

QMAKE_CXXFLAGS += -Wextra -Wshadow -Wformat-nonliteral -Wformat-security -Wtype-limits -Wfatal-errors

OBJECTS_DIR = ./objects
MOC_DIR = ./moc

HEADERS += global.h
HEADERS += mainwindow.h
HEADERS += about_dialog.h
HEADERS += rs232.h
HEADERS += utils.h
HEADERS += tled.h
HEADERS += qt_headers.h

SOURCES += main.cpp
SOURCES += mainwindow.cpp
SOURCES += about_dialog.cpp
SOURCES += rs232.c
SOURCES += utils.c
SOURCES += tled.cpp

unix {
target.path = /usr/bin
target.files = sct
INSTALLS += target
}




