/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2007 - 2022 Teunis van Beelen
*
* Email: teuniz@gmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#ifndef UI_VIEW_MAINFORM_H
#define UI_VIEW_MAINFORM_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <time.h>

#include "global.h"
#include "rs232.h"
#include "about_dialog.h"
#include "utils.h"
#include "tled.h"
#include "qt_headers.h"


#define SERIALPORTTESTER_RCV_BUFSIZE       (4088)

#define SERIALPORTTESTER_INPUT_HISTORY       (16)



class UI_Mainwindow : public QObject
{
  Q_OBJECT

public:
  UI_Mainwindow();


private:

  QDialog      *dialog;

  QTimer       *t1,
               *repeatTimer;

  QLabel       *portLabel,
               *baudrateLabel,
               *modeLabel,
               *DSRLabel,
               *CTSLabel,
               *DCDLabel,
               *RNGLabel,
               *newlineLabel;

  QPushButton  *DTRButton,
               *RTSButton,
               *BREAKButton,
               *openButton,
               *aboutButton;

  QComboBox    *comportComboBox,
               *baudrateComboBox,
               *modeComboBox,
               *newlineComboBox;

  TLed         *ledDTR,
               *ledRTS,
               *ledDSR,
               *ledCTS,
               *ledDCD,
               *ledRNG,
               *ledBREAK;

  QTableWidget *rx_table,
               *tx_table;

  QLineEdit    *inp_lineedit;

  QFont        appfont,
               monofont;

  QAction      *arrow_up_act,
               *arrow_down_act,
               *clear_tx_list_act,
               *clear_rx_list_act;

  QRadioButton *asciiRadioButton,
               *hexRadioButton,
               *binRadioButton;

  QSpinBox     *repeatSpinBox;

  QCheckBox    *repeatCheckBox;

  int comport_nr,
      baudrate,
      statDTR,
      statRTS,
      statDSR,
      statCTS,
      statDCD,
      statRNG,
      statBREAK,
      newline,
      history_idx,
      prevline_idx,
      port_open,
      inputmode;

  char *rx_buf,
       *tx_buf,
       *im_buf,
       mode_str[16],
       input_history[SERIALPORTTESTER_INPUT_HISTORY][SERIALPORTTESTER_RCV_BUFSIZE];

  void clear_lists(void);
  void send_buf(void);

private slots:

  void timer1_handler();
  void show_about_dialog();
  void DTRbutton_clicked();
  void RTSbutton_clicked();
  void BREAKbutton_clicked();
  void dialog_destroyed(QObject *);
  void lineedit_return();
  void newline_changed(int);
  void getPreviousLine();
  void getNextLine();
  void openbutton_clicked();
  void clear_tx_list();
  void clear_rx_list();
  void asciiRadioButtonToggled(bool);
  void hexRadioButtonToggled(bool);
  void binRadioButtonToggled(bool);
  void txtEdited(QString);
  void repeattimer_handler();
  void repeatCheckBoxChanged(int);
  void repeatSpinBoxChanged(int);

protected:

};


#endif



